<!--
---------------------------------------------------------------------------------------------
# FINAL EXAM : Monday 30th January, 2023 : 

### This exam will consist of a multiple choice questionnaire, with the requisite to justify your answer. 
### The answers will not require to use a calculator, and will be based mostly on properties, results or algorithms studied during the lab sessions. 
Therefore : 

## NO DOCUMENT ARE ALLOWED, except a personal handwritten (no copy)  single A4 sheet of paper (‘aide memoire’)


---------------------------------------------------------------------------------------------
/////////////////////////////////////////////////////////////////////////////////////////////

-->

## Welcome to the Machine Learning course (part of 'AI for Autonomous Systems' (ASI 3A and M2 MARS program))

 
## Information

Machine learning courses aim to provide basic knowledge of statistical tools and methods used in a variety of tasks, including data analysis and modeling, classification, clustering and prediction. This knowledge forms the basis for AI developments, while providing the student with the skills needed to critically characterize AI models. 


You will find in this gitlab repository the necessary material for the teaching of _Machine Learning_:

- course materials for the lessons ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/tree/master/slides))
- examples and exercises for the labs in the form of [Jupyter python notebooks](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/tree/master/notebooks) (`.ipynb` files) and/or via online applications,
<!-- - quiz: [Socrative](https://b.socrative.com/login/student/) online tool, room *MLASI* -->

<!-- All necessary information for **installing and running** Python on your computer is given at the bottom of this document. -->

All **assignments** will be indicated below (after the timetable), as the sessions progress.

First course session 2024-2025 will take place Monday, September 23 (check ADE for room and precise time schedule)

## IMPORTANT : 
- **All lectures are expected to be prepared** (reading slides and preparing questions)
- **All lab works must be prepared in advance**. Preparation and assigned homework will be specified a few days before each lab session.
- **Physical presence is compulsory during lab sessions. Reports will be due after each lab session**. These reports enter the evaluation/grading for 50%, and cannot be reconducted for a second session. Therefore, a low grade obatined for the lab sessions implies (almost) systematicaly a 'fail' decision for the module.  
- **AFTER EACH LAB SESSION, your lab reports must be uploaded [here](https://chamilo.grenoble-inp.fr/main/work/work.php?cidReq=ENSE3WEUMAIA0&id_session=0&gidReq=0&gradebook=0&origin=&category_id=0&from_group_category=0)** in the folder corresponding to the lab session number and to your group number; delays may be accepted, though not exceding a few hours. 

## NOTICE : 
- **Due to a lack of computer rooms, lab work will sometimes take place in rooms that are not equipped. Students are invited to bring their own laptops to lab work sessions. You will find below all necessary instructions to run the lab work codes on your own computer.**

`Lab timetables, 2024-2025`

|Group | Supervisor     | Members                                                       | Lab1  | Lab2   | Lab3  | Lab4  | Lab5  | Lab6   | Lab7  | Lab8   | Lab9  |
|-------|:--------------|---------------------------------------------------------------|-------|--------|-------|-------|-------|--------|-------|--------|-------|
|**G1** |  F.Chatelain  |**ASI** students: from AEBISCHER  to LECLERC , french speaking     | 30 sept | 14 sept | 4 nov | 25 nov| 9 dec | 16 dec | 6 janv | 13 jan | 20 jan | |
|**G2** | JM. Brossier  |**ASI** students: from NEFOUSSI to YACINE, french speaking        | 30 sept | 14 sept | 4 nov | 25 nov| 16 dec | 6 janv | 6 janv | 13 jan | 20 jan | |
|**G3** | O. Michel     |**MARS** + foreign(exchange) students - **english speaking**     | 30 sept | 14 sept | 4 nov | 25 nov | 9 dec | 16 dec | 6 janv | 13 jan | 20 jan | | 



### Notice : 
For attending the labs in another group, please send mail to olivier.michel@grenoble-inp.fr.
Note that acceptance of such change is conditionned by tha fact that groups must remain balanced. 



## ----- ASSIGNMENTS ------

##### Lab8 (Jan. 13th, 2025)

- Reread the courses notes on MLPs, NNs and recurrent networks (see HW for dec 12th). 
- Notebooks on Perceptrons are[here]((https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/tree/master/notebooks/10_NN_MLPC).
- Upload at the end of the session your lab 8 short report in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=ENSE3WEUMAIA0&id_session=0&gidReq=0&gradebook=0&origin=&id=145522&isStudentView=true) (pdf file from your editor, or scanned pdf file of a handwritten paper; *code, figures or graphics are not required!*)

##### Lab7 (Jan. 6th, 2025)

- **HW: To do before the lab session:** re-read the
the lesson ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/slides/9_Trees_RandomForest_Boosting.pdf)) on Classification and Regression Trees, tree pruning, Random Forests.
- Lab7 statement on Tree based method for unsupervised classification and regression problems is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/tree/master/labs/lab7_statement.md)
~~ - Upload at the end of the session your lab 7 short report in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=ENSE3WEUMAIA0&id_session=0&gidReq=0&gradebook=0&origin=&id=203024&isStudentView=true) (pdf file from your editor, or scanned pdf file of a handwritten paper; *code, figures or graphics are not required!* ~~


##### Lab6 (Monday, dec 16th 2024, 11:00-13:00) 
- **HW: To do before the lab session:** re-read the
the lesson ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/slides/8_clustering.pdf)) on clustering methods, with focus on K-Means and EM algorithm. Re-read slides 1 to 37.
- Lab6 statement on clustering methods is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/tree/master/labs/lab6_statement.md)
- Upload at the end of the session your lab 6 short report in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=ENSE3WEUMAIA0&id_session=0&gidReq=0&gradebook=0&origin=&id=153447&isStudentView=true) (pdf file from your editor, or scanned pdf file of a handwritten paper; *code, figures or graphics are not required!*)


##### Lecture  (Monday, dec 16th 2024, 8:45-10:45)
- HW : read the lesson on Perceptron, MultiLayer perceptron and Neural Networks (slides are [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/slides/10_MLPC_NN_2020.pdf)). Slides 28 to 37 may be skipped infirst reading.
- Prepare your questions for the course and lab sessions.

##### Lab5 (Monday, dec 9th 2024)
- **HW: To do before the lab session:** re-read the
the lesson ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/slides/6_linear_models_regularization.pdf)) on the lasso regularization and logistic regression parts.
- Lab5 statement on generalized linear models, lasso regulariation and variable selection is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/tree/master/labs/lab5_statement.md)
- Upload at the end of the session your lab 5 short report in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=ENSE3WEUMAIA0&id_session=0&gidReq=0&gradebook=0&origin=&id=145522&isStudentView=true) (pdf file from your editor, or scanned pdf file of a handwritten paper; *code, figures or graphics are not required!*)

##### Lecture (Monday, dec 2nd 2024) on Trees and Boosting~~
- HW: read the lesson on trees and Boosting (slides are [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/slides/9_Trees_RandomForest_Boosting.pdf)). Slides 28 to 37 may be skipped in first reading.
- Prepare your questions for the course and lab sessions.


##### Lab4 (Monday, Nov 25th, 2024) 
!!Reminder : Labworks must be prepared before you attend the lab session.!!
- re-read the lesson ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/slides/6_linear_models_regularization.pdf)) on linear models until ridge regularization.
- lab4 statement on linear models and ridge regression is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/tree/master/labs/lab4_statement.md).
- upload at the end of the session your lab 4 short report in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=ENSE3WEUMAIA0&id_session=0&gidReq=0&gradebook=0&origin=&id=260720&isStudentView=true) (pdf file from your editor, or scanned pdf file of a handwritten paper; *code, figures or graphics are not required!*)


##### Lecture (Monday, November 18, 2024) on Clustering

- Slides on clustering are [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/slides/8_clustering.pdf).
Read slides up to the 'Kernel K-Means' section  and prepare your questions.



##### Homework Lab3 (Monday November 4th)
!!Reminder : Labworks must be prepared before you attend the lab session.!!

- Lab3 statement on Principal Component Analysis (PCA) is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/tree/master/labs/lab3_statement.md)
- upload at the end of the session your lab 3 short report in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=ENSE3WEUMAIA0&id_session=0&gidReq=0&gradebook=0&origin=&id=260369&isStudentView=true) (pdf file from your editor, or scanned pdf file of a handwritten paper; *code, figures or graphics are not required!*)


##### Lecture (Monday, October 21) on linear models and regularization

- Slides on linear models and regularization are [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/slides/6_linear_models_regularization.pdf).

#####  Homework Lab2 (Monday oct 14th) 
!!Reminder : Labworks must be prepared before you attend the lab session.!!
 
- Complete Lab1  notebooks if necessary.  
- Lab2 statement on Discriminant Analysis (LDA/QDA) and Naive Bayes is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/tree/master/labs/lab2_statement.md)
- Upload at the end of the session your lab 2 short report in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=ENSE3WEUMAIA0&id_session=0&gidReq=0&gradebook=0&origin=&id=252078&isStudentView=true) (pdf file from your editor, or scanned pdf file of a handwritten paper; *code, figures or graphics are not required!*)

##### Lecture 2 (Monday, October 7th) on discrimininant analysis & principal component analysis

- Slides on discriminant analysis are [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/slides/4_discriminant_analysis.pdf).
- Slides on principal component analysis (PCA)  are [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/slides/5_principal_component_analysis.pdf).

##### Lab1 (Monday, Sept 30th) instructions

- Lab1 statement on ML basics, k-NN and model assesment is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/tree/master/labs/lab1_statement.md)
- Upload at the end of the session your lab 1 short report in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?id=293985&cidReq=ENSE3WEUMAIA0&id_session=0&gidReq=0&gradebook=0&origin=&isStudentView=true) (pdf file from your editor, or scanned pdf file of a handwritten paper; *code, figures or graphics are not required!*)

##### Lecture 1 Monday, sept 23rd

- read/run the [introductory notebooks to python](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/tree/master/notebooks%2F0_python_in_a_nutshell)  (especially for those who are not comfortable with python)
- **read the lesson** ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/slides/3_model_assesment.pdf)) on model assessment and validation
- **finish reading the lesson** ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/slides/2_illustrations_knn.pdf)) on the k-NN example from slide 7 to 12
- prepare your questions for the course or lab session!


<!--
## `News`

- Because of the containment, there is a zoom link (see the [chamilo page](https://chamilo.grenoble-inp.fr/courses/ENSE3WEUMAIA0/index.php?) of the course) to participate in videoconference to the class every monday morning from 8:30 to 12:45. ___WARNING___ : Pr. CHATELAIN and MICHEL use different zoom link. Please refer to [Chamilo page](https://chamilo.grenoble-inp.fr/courses/ENSE3WEUMAIA0/index.php?).

##### Monday, January 11th 2021
 This last lectures will be focused on CNN and AE principles. The correspoding slides are to be found at the end of the lectures on MLPC_NN used last week.  In a second part, a brief overview on Recurent networks will be given.
 The lectre will befin at 10. Note that corresponding notebooks are given for illustration. No report are required for this session.





##### Lab9

 The last lab session  will be devoted to revisions. You will be able to work on the notebooks that you did not have time to complete in the previous sessions, and tackle the questions of your choice.

##### Lab8 (Monday, January 16th, 2021) 

- Reread the courses notes on MLPs, NNs and recurrent networks (see HW for dec 12th). 
- Lab6 Notebooks on Perceptrons are[here]((https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/tree/master/notebooks/10_NN_MLPC).
- **Except if your supervisor give you another instruction** : Upload at the end of the session your lab 6 short report in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=ENSE3WEUMAIA0&id_session=0&gidReq=0&gradebook=0&origin=&id=145522&isStudentView=true) (pdf file from your editor, or scanned pdf file of a handwritten paper; *code, figures or graphics are not required!*)

##### Lab7 (Jan. 9th, 2023)

- **HW: To do before the lab session:** re-read the
the lesson ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/slides/9_Trees_RandomForest_Boosting.pdf)) on Classification and Regression Trees, tree pruning, Random Forests.
- Lab7 statement on Tree based method for unsupervised classification and regression problems is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/tree/master/labs/lab7_statement.md)
- **Except if your supervisor give you another instruction** upload at the end of the session your lab 7 short report in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=ENSE3WEUMAIA0&id_session=0&gidReq=0&gradebook=0&origin=&id=203024&isStudentView=true) (pdf file from your editor, or scanned pdf file of a handwritten paper; *code, figures or graphics are not required!*)

##### Lab6 (dec 19th, 2022)

- **HW: To do before the lab session:** re-read the
the lesson ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/slides/8_clustering.pdf)) on clustering methods, with focus on K-Means and EM algorithm. Read slides 1 to 37.
- Lab6 statement on clustering methods is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/tree/master/labs/lab6_statement.md)
- Upload at the end of the session your lab 6 short report in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=ENSE3WEUMAIA0&id_session=0&gidReq=0&gradebook=0&origin=&id=153447&isStudentView=true) (pdf file from your editor, or scanned pdf file of a handwritten paper; *code, figures or graphics are not required!*)


##### ~~Homework for Monday, dec 12th 2022~~
- ~~read the lesson on Perceptron, MultiLayer perceptron and Neural Networks (slides are [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/slides/10_MLPC_NN_2020.pdf)). Slides 28 to 37 may be skipped infirst reading.~~
- ~~Prepare your questions for the course and lab sessions.~~ 


##### ~~Lab5 (Monday, dec 5th 2022)~~

- ~~HW: To do before the lab session:** re-read the
the lesson ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/slides/6_linear_models_regularization.pdf)) on the lasso regularization and logistic regression parts.~~
- ~~Lab5 statement on generalized linear models, lasso regulariation and variable selection is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/tree/master/labs/lab5_statement.md)~~
- ~~Upload at the end of the session your lab 5 short report in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=ENSE3WEUMAIA0&id_session=0&gidReq=0&gradebook=0&origin=&id=145522&isStudentView=true) (pdf file from your editor, or scanned pdf file of a handwritten paper; *code, figures or graphics are not required!*)~~

##### ~~Homework for Monday, Nov 28th 2022 on Trees and Boosting~~

-~~read the lesson on trees and Boosting (slides are [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/slides/9_Trees_RandomForest_Boosting.pdf)). Slides 28 to 37 may be skipped in first reading.~~
- ~~Prepare your questions for the course and lab sessions.~~

##### ~~Lab4 (Monday, Nov 21st, 2022) : the lab sessions will EXCEPTIONALLY start at 8:00 !~~

- ~~**Homework : To do before the lab session:** re-read the
the lesson ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/slides/6_linear_models_regularization.pdf)) on linear models until ridge regularization.~~
- ~~lab4 statement on linear models and ridge regression is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/tree/master/labs/lab4_statement.md).~~
- ~~upload at the end of the session your lab 4 short report in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=ENSE3WEUMAIA0&id_session=0&gidReq=0&gradebook=0&origin=&id=260720&isStudentView=true) (pdf file from your editor, or scanned pdf file of a handwritten paper; *code, figures or graphics are not required!*)~~

##### ~~Lecture (Monday, November 14, 2022) on Clusterin~~

- ~~Slides on clustering are [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/slides/8_clustering.pdf).~~
~~Read slides up to the 'Kernel K-Means' section  and prepare your questions.~~

##### ~~Lab3~~

- ~~Lab3 statement on Principal Component Analysis (PCA) is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/tree/master/labs/lab3_statement.md)~~
- ~~upload at the end of the session your lab 3 short report in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=ENSE3WEUMAIA0&id_session=0&gidReq=0&gradebook=0&origin=&id=260369&isStudentView=true) (pdf file from your editor, or scanned pdf file of a handwritten paper; *code, figures or graphics are not required!*)~~

##### ~~Lecture (Monday, October 24) on linear models and regularization~~

- ~~Slides on linear models and regularization are [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/slides/6_linear_models_regularization.pdf).~~

##### ~~Lab2 (Monday, October 17)~~

- ~~Lab2 statement on Discriminant Analysis (LDA/QDA) and Naive Bayes is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/tree/master/labs/lab2_statement.md)~~
- ~~Upload at the end of the session your lab 2 short report in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=ENSE3WEUMAIA0&id_session=0&gidReq=0&gradebook=0&origin=&id=252078&isStudentView=true) (pdf file from your editor, or scanned pdf file of a handwritten paper; *code, figures or graphics are not required!*)~~






-->

### ----------------------------------------------------------------------------
### How to use the notebooks?

TThe examples and exercises will be done under python 3.x through [scikit-learn](https://scikit-learn.org/), and also [tensorflow](https://www.tensorflow.org/). These are two of the most widely used machine learning packages.

The _Jupyter Notebooks_ (`.ipynb` files) are programs containing both cells of code (for us Python) and cells of markdown text for the narrative side. These notebooks are often used to explore and analyze data. Their processing is done with a `jupyter-notebook`, or `juypyter-lab` application, which is accessed through a web browser.

In order to run them you have several possibilities:

1. Download the notebooks to run them on your machine. This requires a Python environment (> 3.3), and the Jupyter notebook and scikit-learn packages. It is recommended to install them via the [anaconda](https://www.anaconda.com/downloads) distribution which will directly install all the necessary dependencies.


**Or**

2. Use a `jupyterhub` online service:

  - we recommend the UGA's service, [gricad-jupyter.univ-grenoble-alpes.fr](https://gricad-jupyter.univ-grenoble-alpes.fr/), so that you can run your notebooks on the UGA's computation server while saving your modifications and results. Also useful to launch a background computation (connection with your Agalan account; requires uploading your notebooks+data to the server).
  - alternatively you can use an equivalent `jupyterhub` service. For example the one from google, namely [google-colab](https://colab.research.google.com/), which allows you to run/save your notebooks and also to _share the edition to several collaborators_ (requires a google account and upload your notebooks+data in your Drive)


**Or**

3. `<Deprecated>` Use the _mybinder_ service ans links to run them interactively and remotely (online): [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fchatelaf%2Fml-sicom3a/master?urlpath=lab/tree/notebooks) (open the link and wait a few seconds for the environment to load).<br>
  **Warning:** Binder is meant for _ephemeral_ interactive coding, meaning that your own modifications/codes/results will be lost when your user session will automatically shut down (basically after 10 minutes of inactivity)

**Note :** You will also find among the notebooks an introduction to Python [notebooks/0_python_in_a_nutshell](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/tree/master/notebooks%2F0_python_in_a_nutshell)


### Miscellaneous remarks on the materials

- The slides are designed to be self-sufficient (even if the narrative side is often limited by the format).
- In addition to the slides and bibliographical/web references, we generally propose links or videos (at the beginning or end of the slides) specific to the concepts presented. These lists are of course not exhaustive, and you will find throughout the web many resources, often pedagogical. Feel free to do your own research. 
