# MultiLayer Perceptrons, Neural Networks

The objective of this lab is to introduce basic properties of MultiLayer perceptrons, and to get first insights on the role of different choices for hyperparameters (number of layers, number of neurons, choice of activation functions) and optimization algorithm (quasi Newton or stochastic gradient).

1. Basic properties and first steps with perceptrons ['N1_Perceptron.ipynb'](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/notebooks/10_NN_MLPC/N1_Perceptron.ipynb)

2. First examples of perceptron based classifiers ['N2_MLPClassifier.ipynb'](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/notebooks/10_NN_MLPC/N2_MLPClassifier.ipynb)

3. Example of MLP based regressors, basic properties['N3_MLPRegressor.ipynb'](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/notebooks/10_NN_MLPC/N3_MLPRegressor.ipynb)

Next Notebooks do not contain problem nore exercizes. They are given as illustrations of the preceding methods to more realistic data sets, and to illustrate usage of classical tensorflow Deep Learning algorithms. 

4. MLP based handwritten digit classification: case study on Mnist Database ['N4_MLP_MNIST.ipynb'](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/notebooks/10_NN_MLPC/N4_MLP_MNIST.ipynb)

5. Convolutional network based handwritten digit classification : case study on Mnist Database ['N5_CNN_MNIST.ipynb'](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/notebooks/10_NN_MLPC/N5_CNN_MNIST.ipynb)

6. *Optional*: Transfert learning and fine-tuning, see and comment the [keras tutorial](https://colab.research.google.com/github/keras-team/keras-io/blob/master/guides/ipynb/transfer_learning.ipynb#scrollTo=TEerfpJLRXPm) (google colab notebook)
